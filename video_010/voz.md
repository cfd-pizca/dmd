# Descripción

Este archivo comenzó siendo una copia de la transcripción automática
de youtube. Pero, está aquí para que sea modificado, posiblemente
corregido, o traducido, con la idea de que funcione como guión de un
material que pudiera realizarse posteriormente.

Sería muy bueno que este material posterior usara una voz
digital. Creo que así se podría evitar la edición de video. Es decir,
se podría armar el video final pegando framentos de video para las
ecuaciones, y luego sobreponinedo la voz.

# es

00:02
pero de 7° de notación de índices y en

00:08
este caso vamos a platicar un poquito

00:10
sobre cosas relacionadas con cálculo

00:13
entonces

00:19
comencemos con

00:21
unos ejes así

00:25
x 1

00:28
y x2

00:30
yo voy a estar hablando de tres

00:33
dimensiones espaciales pero solamente

00:35
voy a dibujar dos porque los dibujos son

00:37
más claros entonces este

00:42
pensemos en tres dimensiones espaciales

00:43
pero solo dibujo dos

00:46
ok entonces aquí puedo tener un punto a

00:48
donde sea

00:51
y este punto tiene unas coordenadas x1 y

00:54
x2

00:56
entonces hagamos una colección de puntos

01:01
que la voy a dibujar así como una

01:02
rejilla aquí

01:05
con tres líneas verticales

01:10
y tres líneas

01:12
horizontales

01:16
entonces bueno tenemos mejor que nuestro

01:20
punto

01:21
está aquí y le vamos a poner unas

01:24
coordenadas

01:27
x1 igualados y x2

01:31
igual a 2

01:34
este dibujito de alguna manera va a ser

01:36
nuestra referencia

01:39
y aquí va a haber una base

01:45
orton normal y cartesiana entonces el

01:48
chiste de este dibujito es que yo voy a

01:51
deformar esa rejilla yo sigo teniendo

01:54
mis ejes

01:56
x1 y x2

01:59
derechitos

02:03
pero voy a pensar en algo que pasó de

02:08
tal manera que

02:09
se deforma mi rejilla

02:12
vamos a pensar por lo menos por ejemplo

02:14
así

02:15
ahí está mi punto quedó acá

02:21
entonces ese punto tiene ahora unas

02:24
coordenadas x1 y x2 diferentes x1

02:29
es

02:32
3.1

02:34
y x2

02:38
3.3 porque la

02:43
ahora yo utilizo estas rayitas rojas

02:48
a definir unas variables nuevas estas

02:51
variables las voy a llamar q

02:56
entonces yo voy a decir que de este lado

02:58
mi punto tiene

03:02
y el uno se lo voy a poner arriba

03:06
q no es igual a 2

03:10
y q2 es igual a 2 y lo que quiero decir

03:15
es que estoy en la segunda rayita 12

03:17
aquí

03:19
2 y aquí está mi punto

03:22
y de este lado

03:26
1

03:28
sigue siendo 2 y q2

03:32
sigue siendo 2

03:35
porque este punto sigue estando en la

03:38
segunda rayita aunque ya no está en la

03:40
misma

03:42
x que estaba antes o sea la x1 cambio

03:45
pero sigue estando sobre la segunda

03:47
rayita roja entonces mi q 1 es igual a 2

03:51
y lo mismo para la q2

03:54
salem bueno hago un tercer dibujito acá

03:59
yo puedo pensar en un espacio donde las

04:02
cosas están derechas

04:04
1

04:07
12

04:12
si yo puedo pensar en un espacio donde

04:14
las cosas tan derechas si en algún

04:16
momento puede que este espacio me

04:19
resulte útil para ver alguna cosa que

04:22
esté escrito en términos de las cosas

04:24
luego

04:26
la posición de mi punto

04:29
que es x1

04:31
va a ser diferente dependiendo del lugar

04:35
donde yo lo haya escogido originalmente

04:38
acá entonces x1 va a ser una función

04:43
1

04:45
q2

04:47
y contras

04:50
[Música]

04:52
aunque yo solamente estoy dibujando dos

04:54
coordenadas

04:58
y lo mismo la x2 quiero decir este 3.3

05:03
que está aquí no va a ser lo mismo para

05:05
todos los puntos va a ser un valor

05:07
diferente y ese valor yo lo voy a poder

05:10
determinar sabiendo en donde estaba

05:15
originalmente

05:18
en esta rejilla antes de que se entre

05:20
cara

05:23
entonces de manera genérica yo escribo

05:26
esto como igual a x

05:30
1

05:33
de un cierto vector q

05:35
y de manera un poco más genérica

05:39
con una notación un poquito mezclada yo

05:41
escribo x y es igual a una función de q

05:46
y desde luego esta deformación si es una

05:51
cosa más o menos bien portada yo debería

05:54
de poder escribir

05:56
la cv y como una función de las equis

05:59
eso es la función inversa hasta aquí

06:02
todavía no hay cálculo

06:06
las cosas interesantes empiezan a

06:08
aparecer cuando yo digo bueno en esta

06:10
representación de aquí en medio

06:13
yo voy a pensar en una cierta variable

06:16
fin

06:17
puede ser por ejemplo la temperatura la

06:20
temperatura puede estar definida en este

06:22
punto en este punto en este punto en

06:24
este punto en este punto

06:26
yo puedo pensar en principio en la

06:29
temperatura

06:32
como una función de las variables x

06:37
pero como esas variables x yo las tengo

06:41
definidas en términos de las otras

06:43
variables

06:45
entonces

06:48
yo puedo escribir aquí esa dependencia y

06:51
me queda la misma variable fi escrita en

06:54
términos de las variables q

07:00
al principio

07:06
con mi función escrita de esta manera

07:11
yo me puedo preguntar cuánto vale la

07:13
derivada con respecto a alguna de las

07:14
coordenadas x

07:18
y eso tiene un cierto valor si yo pienso

07:22
en mi fe escrita de esta manera lo cual

07:25
abusando un poquito de la anotación

07:28
podemos escribir

07:31
este de aquí es un abuso de anotación

07:36
entonces yo puedo preguntarme cuánto

07:40
vale

07:42
la fe con respecto a mí

07:47
y la derivada de la fi con respecto a mi

07:49
cuit y desde luego la manera de resolver

07:51
esto es aplicando la regla de la cadena

07:55
de la fe con respecto a la x 1

08:00
luego la derivada de la x1 con respecto

08:04
a la variable con la que yo esté

08:07
trabajando o sea la que quiero con

08:11
respecto a la que quiero derivar

08:13
luego más

08:17
con respecto x2 la parcial del x 2 con

08:21
respecto

08:24
variable con la que yo quiero derivar

08:27
y la parcial

08:30
x 3

08:32
con respecto y la parcial de x 3 con

08:36
respecto a la variable que yo quiero que

08:38
es la cookie si esta es la regla de la

08:41
cadena aplicada tal cual de la

08:44
definición este para funciones de tres

08:49
variables

08:52
entonces yo sé cómo escribir esto de una

08:55
manera más compacta

09:00
y se ve así la parcial

09:05
q

09:13
entonces yo tengo tres cosas

09:17
yo tengo lo que ya conocía

09:22
esto de aquí ya lo conocía lo que quiero

09:25
conocer esto de acá

09:30
aquí algo que es lo que me está diciendo

09:33
cómo obtener lo que quiero en términos

09:36
de lo que ya conocía

09:38
entonces utilicemos este tercer oeste

09:43
para definir una base

09:50
esta base sin gorro

09:53
la defino en términos de la base que yo

09:55
tenía antes

09:57
qué es la base con gorro justamente

10:01
utilizando

10:04
a este escenario

10:08
[Música]

10:10
y entonces a esta cosa le dice red y

10:12
base unitaria

10:18
lo cual a mí no me gusta porque

10:21
los elementos de esta base de acá no la

10:25
magnitud del elemento no es 1

10:27
justo por eso no le puse gorro

10:30
porque porque la magnitud de estos si es

10:33
1 pero estas derivadas en general no van

10:35
a ser 1 entonces esta base de acá pues

10:39
en general no va a tener elementos

10:40
unitarios pero bueno se llama base

10:43
unitaria

10:46
por si en algún momento no lo encuentran

10:48
tomemos un momentito para ver esta nueva

10:53
base que yo acabo de definir

10:56
como se ve en términos geométricos el

10:59
ejemplo que se me ocurre más sencillo de

11:01
todos

11:03
aquí hacemos un paréntesis

11:06
sería x 1 igual a 1

11:11
x2 igual la q2

11:15
entonces esta derivada vale 1 cuando la

11:19
jota y la i valen 1 y vale 1 cuando las

11:22
dos son dos entonces uno es igual

11:29
corto corto normal

11:33
2

11:35
es igual

11:38
en términos de los dibujitos yo tenía

11:43
tres rayas 1

11:45
tres rayas y esa es la rejilla no cambió

11:49
nada

11:51
tengo una dos tres rayas y una dos tres

11:54
rayas

11:57
entonces digamos la base como estaba acá

12:00
se ve igual que la base de acá no pasa

12:03
nada

12:09
otro ejemplo muy muy sencillito sería

12:14
x 1 igual a dos veces q 1

12:19
y x2 igual

12:23
así tal cual

12:25
en este caso la 1

12:29
va a ser igual

12:32
dos veces

12:35
por todo normal

12:38
y las dos va a ser igual a las dos auto

12:41
normal

12:43
hasta entonces en términos del dibujito

12:45
yo tenía

12:48
mi rejilla derechita sin deformarse y lo

12:54
que va a pasar es que la nueva

12:57
coordenada x1 va a estar dos veces más

13:00
lejos de lo que estaba originalmente

13:04
entonces si esto está aquí esto va a

13:07
quedar por acá y esto va a quedar por

13:10
acá

13:12
y esto va a quedar igualmente espaciado

13:15
como estaban

13:17
bueno entonces eso es un ejemplo muy

13:22
sencillito para tener una mínima

13:24
intuición geométrica

13:29
utilizar este bicho para definir una

13:31
nueva base me dice algo sobre cómo

13:34
cambia el dibujito

13:36
y listo 


