# Descripción en youtube

Conceptos discutidos en este video:
- mapeo
- coordenadas en configuración de referencia (materiales)
- coordenadas en configuración deformada (espaciales)
- campo de velocidad
- abuso de notación, mismo signo para diferentes funciones y coordenadas
- descripción espacial (euleriana)
- descripción material (lagrangiana)
- derivada material

# Observaciones

(pendiente: hacer issues para todo esto)

0:38 pordría quitarse "que es lo que estamos haciendo"

No tiene "escritura acelerada"

Si tiene "escritura aparecida"

5:36 error "podemos coordenar" 

El mouse casi no se ve

Ejemplo de brincos en la cara, de 9:10 a 10:15

se oye un poquito de riudo hiss

Hay anotaciones laterales

Sería mejor usar la letra v para velocidad, no la letra u

15:31 Alusión a las sesiones en vivo

22:30 Se dice "con respecto las derivadas x", debería decir "con respecto a las coordenadas x"

23:00 Se escribe un signo de derivada parcial y debería ser una D

25:25 Posiblemente valdría la pena decir que en dinámica de fluidos
son muy comunes las coordenadas eulerianas, mientras que en sólidos
elásticos, son más comunes las lagrangianas.


