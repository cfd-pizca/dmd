# Descripción

[Estos](.) son algunos archivos complementarios a algunos videos que se
usaron en el curso de "Dinámica de Medios Deformables", impartido a
finales de 2020, por Roberto Velasco Segura y Federico Hernández
Sánchez, en la Facultad de Ciencias, de la Universisdad Nacional
Autónoma de México.

Los videos se pueden encontrar en [esta lista de youtube](https://www.youtube.com/playlist?list=PLf1MPu5-32TIKHmPdLhKEXtTwAAbg5vRR).

Aún cuando se puso todo el cuidado posible al realizar estos videos,
tienen algunos errores, unos pequeños y otros no tan pequeños. En un
espíritu de buscar que se realice un siguiente material, son muy
bienvenidas las observaciones, de todo tipo, pero **especialmente las
que indiquen errores de contenido**.

Se aceptan los comentarios por cualquier via (email, comentarios de
youtube, etc.), pero se recomienda el uso de [issues de
gitlab](https://gitlab.com/cfd-hunks/dmd/-/issues). Con esto se busca que estén juntos los comentarios al
respecto de un mismo punto, y que sean públicos.

El contenido sigue una estructura basada en el texto de J. N. Reddy
"An Introduction to Continuum Mechanics" (primera edición, primeros 6
capítulos). Se procura que la notación sea consistente. Sin emabrgo,
no se presenta todo el material. Además, se han tomado algunas
decisiones diferentes al momento de deducir los contenidos, y al
respecto de los puntos que se resaltan.

El uso de este formato grabado, fue consecencia de la situación de
emergencia que se vive en todo el mundo, buscando promover el trabajo
asíncrono. Sin emabrgo, por la premura, no hay un entrenamiento previo
para muchos aspectos del formato. Esto incluye:
- el proceso de grabación,
- el proceso de edición,
- las herramientas de pizarrón digital.

Entonces, se fue experimentando a medida que se avanzaba en los temas.
Por lo tanto, hay cosas que no funcionan del todo bien. Sea entonces
este un testimonio, que posiblemente incluye algunos ejemplos de lo que 
que **no** se recomienda para este tipo de material.

En un espíritu de que se puedan analizar, y posiblemente optimizar,
los detalles de estos procesos
- [se describen](
./sobre_el_proceso.md) las herramientas usadas en cada uno de los videos,
  y
- se proporcionan (dentro de los directorios de cada video) algunos de
  los materiales que fueron usados, cuando es posible, en los
  [formatos](./formatos.md) editables.


