(esto está incompleto)

Espero que estos comentarios sean valiosos para una persona, como yo,
**no** entrenada en:
- grabación de video, 
- edición de video, 
- uso de software para tabletas de dibujo y notas manuales.

# 1. Pizarrón electrónico

Creo que es mejor no recorrer la pantalla (scrollear), sino tener
láminas. La edición de video es más fácil, especialmente cuando hay
que corregir un error de lo que está escrito.

# 2. Grabación

## 2.1 Grabación del mouse

Facilita la indicación de alguna parte de la pantalla

Dificulta el aumento de velocidad en la escritura de ecuaciones,
brinca demasiado.

## 2.2 Aparición del presentador a lo largo del video

Dificulta mucho la edición. Algunas veces conviene desfasar el audio
con el video, si está el rostro del presentador esto se dificulta,
porque los labios no corresponden con lo que se escucha.

Al menos en OBS, no es posible garbar el video del presentador por
separado, para luego sobreponerlo en edición.

En el mismo sentido, algunas veces los reproductores de video (locales
o en línea) desfasan un poco el audio con le video. Cuando el rostro
del presentado está en el video, este desfase es incómodo y
distrae. Cuando el presentador está ausente, este desfase
prácticamente no se nota.

Un cuadro pequeño es incómodo, no te puedes mover.

Se pensó que el lenguaje no verbal del presentador (expresiones
faciales) podría ayudar a la descripción de los materiales
presentados. Finalmente, me parece que no ayuda mucho, con la voz se
logra muy bien esta parte.

Finalmente, se decidió poner al presentador solamente en la portada,
lo cual
- evita las dificultades mencionadas,
- pretende hacer que la presentación sea un poco más cercana.

### 2.2.1 Pantalla verde

Si hay una ventana, donde entre luz de día, hay que ajustar el
parámetro NNN cada vez que se comienza la grabación.

Se requiere una superficie grande. Lo hice con un pizarrón de 1.20 x 1
[m] y resultó suficiente solamente con un cuadro muy cerrado.

En los videos 32 al 34 no se usa pantalla verde porque el pizarrón
quedaba chico, y no tenía a la mano otra cosa.

## 2.3 Dos monitores

Ver lo que se está grabando, o al menos el lugar donde se indica que
efectivamente está grabando y el nivel de sonido.

## 2.4 Pausas en la grabación

## 2.5 Escribir al momento de hablar

## 2.6 Pausas al hablar

## 2.7 Notas como archivo adicional

Resultó bueno tener este tipo de notas:
- para buscar donde quedó alguno de los temas
- para recordar lo que se había dicho 

Resultó más manejable un pdf que un png, aunque posiblente podría
haber sido mejor un epub.

## 2.8 Formato de video

Si la grabación se hace en formato mkv
- la edición resulta más fácil.
- es posible ver el video (por ejemplo en mpv), cuando la grabación
  está pausada.



# 3. Edición de video

Intenté varias cosas diferentes, pero siempre resultó un proceso
sumamente largo. Hasta de una hora por cada dos minutos de video
final. Usar 13 horas de edición para 25 min de video no es óptimo,
posiblemente haya una manera mejor.

Esencialmente es cortar pedazos.

Es fácil cortar más de lo que deberías y la voz se escucha poco
natural.

Si la grabación se hace en formato mkv la edición resulta más fácil.

## 3.1 Corregir cosas

El audio se escucha raro cuando grabas algo a posteriori, aunque sea
el mismo programa, el mismo micro. Me imagino que hay que ser muy
bueno para darle a la voz la "misma intención".

Los letreros creo que funcionan razonablemente bien y son rápidos de
hacer.

Es posible cambiar un signo y otras cosas sencillas, sobreponiendo una
imagen al video principal.

## 3.2 Extensión del material editado

Se observó que el tiempo del material grabado se reduce
considerablemente cuando se hace edición. Típicamente a la mitad, pero
en casos llega a reducirse hasta a una tercera parte del tiempo de
grabación.

En particular, para una clase que sería una sesión de dos horas,
resulta muy difícil ofrecer dos horas de material editado. Además, si
así fuera, la cantidad de material sería mucho mayor que la que podría
ser presentada en una clase. Entonces, se ofreció finalmente un
aproximado de 40 min de material editado, de manera correspondiente a
una clase de dos horas.

Por otra parte, se observó que si los videos son más pequeños, los
estudiantes los aprovechan mejor. Entonces se procuró ofrecer dos
videos de 20 min en lugar de uno de 40 minutos. Esto, además ayuda a
identificar los temas en cada uno de los videos.

# 4. Audio

Francamente, no experimenté, 
- diferentes micrófonos
- filtros en la grabación
- filtros en la edición.

Me quedé con el default. 

Creo que al menos debería aplicarle un filtro de ruido blanco.

# 5. Herramientas de software

Este material fue realizado 100% con software libre. Muchas gracias a
los desarrolladores.

## 5.1 Kdenlive

No se especifica abajo porque se usó en todos los videos.

## 5.2 OBS

Muy bueno.

Mucho mejor si es posible conectar dos pantallas.

Sería muy bueno que pudieras previsualizar directamente ahí, cuando la
grbación está pausada y borrar un pedazo de lo que ya grabaste.

No se especifica abajo porque se usó en todos los videos.

## 5.3 xournal

Tanto xournal como xournalpp son diferentes a mypaint en lo
siguiente. My paint registra pixeles, y los otros dos registran
"trazos vectoriales". Es la misma diferencia que hay entre photoshop e
illustrator, o entr gimp e inkscape.

Como consecuencia, enxurnal y xournalpp es más fácil borrar, copiar,
pegar, mover trazos completos aunque estén ensimados con otros trazos.

Hubo algún detalle que no tenía, por eso me cambié a xournalpp, no me
acuerdo qué fue.

## 5.4 xournalpp

Es bueno usar shortcuts (key bindings)

Mi configuración está [aquí](./conf/xournalpp/).

Tiene layers pero no se usaron.

El sistema de audio falló, por eso no se usó. Se hicieron varias
pruebas. 

Sería muy bueno poder agrupar objetos y mandarlos a otra lámina sin arrastrarlos.

A veces se confunde y manda "segmentation fault"

La versiónd de flatpak por ahora no acepta plugins.

No está en el repositorio oficial de debian.

Sería bueno un exportado a epub o a html.

Sería bueno que tuviera una herrmienta como el laser de jamboard. Lo
que se hizo fue usar la herramienta "highlight" y borrar, cada vez.

No esposible seleccionar al mismo tiempo dos fragmentos de texto que
están en páginas diferentes. Esto es inconveniente cuando tratas de
seleccionar un fragmento detexto muy pegado a la parte de arriba de
una página, y comienzas arrastrando el mouse desde la página de
arriba.

Cuando copias y pegas el trazo deja de ser editable, se convierte en
una imagen png. Esot es inconveniente cuando se requiere copiar ua
ecuación desde la página 1 hasta la página 7, por ahora hay que
arrastrarla por las páginas intermedias. Lo hace aun más inconveniente
que es fácil perder la selección, apor ejemplo al tocar algo, queda
ansimado y se necesita comenzar de nuevo. La mejor solución que
encontré es "scrollear" con el mouse (o el trackpad) mientras se
arrastra la ecuación con la tableta.

## 5.5 MyPaint

El trazo es más expresivo.

No tiene una manera de mover un pedazo (una ecuación) rápidamente.

El exportado no es muy cómodo, queda un png muy grande.

Si se convierte este png a un pdf la letra es muy chica.

No permite escribir texto desde el teclado, o no es fácil.

## 5.6 mpv

Permite ver el video al mismo momento que se está grabando. Es
decir:
- pones pausa en OBS
- abres el archivo de video que se está grabando en mpv
- revisas alguna cosa que se haya grabado
- si es necesario regresar, puedes usar mpv para poner tu pizarrón
  electrónico como lo tenías antes (especialmente si estás usando
  scroll)
- si sigues grabando no es necesario cerrar el archvio de video (en
  mpv) y volverlo a abrir (en otros visores de video sí es necesario)

## 5.7 Paraview

No se cómo seleccionar caras, por eso hice un artilugio para que
fueran de colores difrentes (video 25).

## 5.8 Blender

No se usó, pero se debería haber usado. 

Si se usara blender, muchos de los esquemas podrían ser más claros
(incluso el que se hizo en paraview).

## 5.9 Debian

Se usó como sistema operativo GNU/Debian 10, pero todas las
herraminetas de software que aquí se describen están disponibles para
otros sistemas operativos como:
- Windows
- OSX
- Ubuntu

La versión Debian 10 (buster) trae en el kernel los drivers para la
tablet que yo usaba (wacom intuos wired). Es decir, no hay que hacer
nada, simplemente plug and play. La versión 9 (stretch) no los
trae. Creo que lo importante es la versión del kernel, que 5 o más
reciente.

Casi todos los paquetes de software que se enlistan arriba, fueron
instalados directamente de los repositorios de debian, con
apt. Excepto xournalpp. Este se puede instalar de flatpak, pero no ha
logrado que los plugins funcionen, entonces lo descargué directo de
github.

# 6. Detalles de formato en los videos

## video  1 
- MyPaint
- Inserto de presentador, permanente
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video  4 
- MyPaint
- Inserto de presentador, permanente
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video  5 
- MyPaint
- Inserto de presentador, permanente
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video  6 
- MyPaint
- Inserto de presentador, permanente
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video  7 
- MyPaint
- Inserto de presentador, permanente
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video  8 
- MyPaint
- Inserto de presentador, permanente
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video  9 
- MyPaint
- Inserto de presentador, permanente
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video 10 
- MyPaint
- Inserto de presentador, permanente
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video 11 
- MyPaint
- Inserto de presentador, permanente
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video 12 
- MyPaint
- Inserto de presentador, permanente
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video 13 
- MyPaint
- Inserto de presentador, permanente
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video 14 
- MyPaint
- Inserto de presentador, permanente
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video 15 
- MyPaint
- Inserto de presentador, permanente
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video 16 
- MyPaint
- Inserto de presentador, permanente
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video 17 
- xournal
- Inserto de presentador, permanente
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video 18 
- xournal
- Inserto de presentador, permanente
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video 19 
- xournal
- Inserto de presentador, permanente
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video 20 
- xournal
- Inserto de presentador, permanente
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video 21 
- xournal
- Inserto de presentador, permanente
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video 22 
- xournal
- Inserto de presentador, permanente
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video 23 
- xournal
- Inserto de presentador, permanente
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video 24 
- xournal
- Inserto de presentador, permanente
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video 25 
- xournalpp
- paraview
- Inserto de presentador, permanente
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video 26 
- xournalpp
- Sin inserto de presentador
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video 27 
- xournalpp
- Sin inserto de presentador
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video 28 
- xournalpp
- Sin inserto de presentador
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video 29 
- xournalpp
- Sin inserto de presentador
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video 30 
- xournalpp
- Sin inserto de presentador
- Arrastrado de pantalla (scroll)
- Se muestra el "mouse" del presentador
## video 31 
- xournalpp
- Sin inserto de presentador
- Aumento de velocidad al escribir texto
- Arrastrado de pantalla (scroll)
- No se muestra el "mouse" del presentador
## video 32 
- xournalpp
- Inserto de presentador, en portada
- Aumento de velocidad al escribir texto
- Láminas (no scroll)
- No se muestra el "mouse" del presentador
## video 33 
- xournalpp
- Inserto de presentador, en portada
- Aumento de velocidad al escribir texto
- Láminas (no scroll)
- No se muestra el "mouse" del presentador
## video 34 
- xournalpp
- Inserto de presentador, en portada
- Aumento de velocidad al escribir texto
- Láminas (no scroll)
- Duplicado de fragmentos
- No se muestra el "mouse" del presentador
